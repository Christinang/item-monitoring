<?php
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    

    try
    {
        $query = $conn->prepare('SELECT * from USR');
        $query->execute();
        $check = $query->setFetchMode(PDO::FETCH_ASSOC); 
        if($check == true)
        {
            $row = $query->fetchAll();
            if($_GET['type'] == 'table')
            {
                _displayTable($row);
            }
            else if($_GET['type'] == 'option')
            {
                echo json_encode($row);
            }
        }


    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Oops', 'url' => '');
        // echo $e->getMessage();
    }

    function _displayTable($row)
    {
        echo '
        <table id="listofUser" class="table table-bordered table-nowrap dataTable">
            
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach($row as $k=>$v) { 
            echo '
             <tr>
                <td>'.$v['usr_fullname'].'</td>
                <td>'.$v['usr_username'].'</td>
                <td>'.$v['usr_password'].'</td>
                <td>
                    <div>
                        <button class="btn btn-warning btn-sm edit_user"><i class="icon icon-pencil"></i> Edit</button>
                    </div>
                </td>
            </tr>';
        }

        echo '
            </tbody>
        </table>
        ';
    }
    
?>