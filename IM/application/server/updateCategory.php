<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $category_id = $_POST['category_id'];
    $category_name = $_POST['category_description'];

    try
    {
        if(!empty($category_name)){
            $query = $conn->prepare('UPDATE CATEGORY set category_name="'.$category_name.'" where category_id="'.$category_id.'"');
            if(!_check_category_duplication($conn,$category_name))
            {
                $query->execute();
                $result = array('flag' => '1', 'message' => 'Successfully edited category!', 'url' => 'supplier.html');
            }
            else
            {
                $result = array('flag' => '2', 'message' => 'Nothing Change', 'url' => 'supplier');
            }
        }
        else{
            $result = array('flag' => '3', 'message' => 'Must fill all', 'url' => 'category');
        }
        
    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in editing category', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>