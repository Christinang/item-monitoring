<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $category_name = $_POST['description'];

    
    try
    {
        
        $query = $conn->prepare('INSERT into CATEGORY (category_id,category_name,created_by) VALUES(null,"'.$category_name.'","'.$_SESSION['id'].'")');
        if(!_check_category_duplication($conn,$category_name))
        {
            $query->execute();
            $result = array('flag' => '1', 'message' => 'Successfully added category!', 'url' => 'category.html');
        }
        else
        {
            $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'category');
        }
    }
        
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in adding category', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>