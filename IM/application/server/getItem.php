<?php
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    

    try
    {
        $query = $conn->prepare('SELECT * from displayItem');
        $query->execute();
        $check = $query->setFetchMode(PDO::FETCH_ASSOC); 
        if($check == true)
        {
            $row = $query->fetchAll();
            if($_GET['type'] == 'table')
            {
                _displayTable($row);
            }
            else if($_GET['type'] == 'option')
            {
                echo json_encode($row);
            }
        }


    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Oops', 'url' => '');
        // echo $e->getMessage();
    }

    function _displayTable($row)
    {
        echo '
        <table id="listofItem" class="table table-bordered table-nowrap dataTable">
            
            <thead>
                <tr>
                    <th>Item Name</th>
                    <th>Item Description</th>
                    <th>Item Price</th>
                    <th>Category</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach($row as $k=>$v) { 
            $temp = $v['item_id'];
            echo '
             <tr>
                <td>'.$v['item_name'].'</td>
                <td>'.$v['item_description'].'</td>
                <td>'.$v['item_price'].'</td>
                <td>'.$v['category_name'].'</td>
                <td>'.$v['usr_fullname'].'</td>
                <td>
                    <div>
                        <button type="submit" name="edit" id="'.$temp.'" data-toggle="tooltip" title="Edit" data-placement="top" onclick="openModal('.$temp.')" class="btn btn-warning btn-sm edit_item"><i class="icon icon-pencil"></i> Edit</button>
                    </div>
                </td>
            </tr>';
        }

        echo '
            </tbody>
        </table>
        ';
    }
    
?>