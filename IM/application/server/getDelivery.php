<?php
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    

    try
    {
        $query = $conn->prepare('SELECT * from delivery');
        $query->execute();
        $check = $query->setFetchMode(PDO::FETCH_ASSOC); 
        if($check == true)
        {
            $row = $query->fetchAll();
            if($_GET['type'] == 'table')
            {
                _displayTable($row);
            }
            else if($_GET['type'] == 'option')
            {
                echo json_encode($row);
            }
        }


    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Oops', 'url' => '');
        // echo $e->getMessage();
    }

    function _displayTable($row)
    {
        echo '
        <table id="listofDeliveredItems" class="table table-bordered table-nowrap dataTable">
            
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Customer Address</th>
                    <th>Customer Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach($row as $k=>$v) { 
            $temp = $v['delivery_id'];
            echo '
             <tr>
                <td>'.$v['delivery_customer'].'</td>
                <td>'.$v['delivery_cus_address'].'</td>
                <td>'.$v['delivery_cus_phone'].'</td>
                <td>
                    <div>
                        <button type="submit" name="edit" id="'.$temp.'" data-toggle="tooltip" title="Edit" data-placement="top" onclick="openModal('.$temp.')" class="btn btn-warning btn-sm edit_delivery"><i class="icon icon-pencil"></i> Edit</button>
                    </div>
                </td>
            </tr>';
        }

        echo '
            </tbody>
        </table>
        ';
    }
    
?>