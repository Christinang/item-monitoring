<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $sup_id = $_POST['supplier_id'];
    $sup_name = $_POST['supplier_name'];
    $sup_address = $_POST['supplier_address'];
    $sup_contactNo = $_POST['supplier_contactNo'];

    try
    {
        $query = $conn->prepare('UPDATE SUPPLIER set sup_name="'.$sup_name.'", sup_address="'.$sup_address.'", sup_contactNo="'.$sup_contactNo.'" where sup_id="'.$sup_id.'"');
        if(!_check_supplier_duplication($conn,$sup_name,$sup_address,$sup_contactNo))
        {
            $query->execute();
            $result = array('flag' => '1', 'message' => 'Successfully edited supplier!', 'url' => 'supplier.html');
        }
        else
        {
            $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'supplier');
        }   
    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in editing supplier', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>