<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $delivery_customer = $_POST['name'];
    $delivery_cus_address = $_POST['address'];
    $delivery_cus_phone = $_POST['phone'];

    
    try
    {
        if(!empty($delivery_customer)&&($delivery_cus_address)&&($delivery_cus_phone)){
            $query = $conn->prepare('INSERT into delivery (delivery_id,delivery_customer,delivery_cus_address,delivery_cus_phone) VALUES(null,"'.$delivery_customer.'","'.$delivery_cus_address.'","'.$delivery_cus_phone.'")');
            if(!_check_category_duplication($conn,$delivery_customer,$delivery_cus_address,$delivery_cus_phone))
            {
                $query->execute();
                $result = array('flag' => '1', 'message' => 'Successfully added catedeliverygory', 'url' => 'delivery.html');
            }
            else
            {
                $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'delivery');
            } 
        }
        else{
            $result = array('flag' => '3', 'message' => 'Must fill all fields', 'url' => 'delivery');
        }
    }
        
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in adding delivery', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>