<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $item_id = $_POST['item_id'];
    $category_id = $_POST['category_select_option1'];
    $item_name = $_POST['item_name'];
    $item_description = $_POST['item_description'];
    $item_price = $_POST['item_price'];

    $getCategoryID = $conn->prepare('SELECT category_id from category where category_name = "'.$category_id.'"');
    $getCategoryID->execute();
    $check = $getCategoryID->setFetchMode(PDO::FETCH_ASSOC);
    $catID = $getCategoryID->fetchAll();


    foreach ($catID as $key => $value) {
    }
    try
    {
        if(!empty($item_name)&&($item_description)&&($item_price))
        {
            $query = $conn->prepare('UPDATE ITEM set category_id="'.$value['category_id'].'", item_name="'.$item_name.'", item_description="'.$item_description.'", item_price="'.$item_price.'", created_by="'.$_SESSION['id'].'" where item_id="'.$item_id.'"');
            if(!_check_item_duplication($conn,$category_id,$item_name,$item_description,$item_price))
            {
                $query->execute();
                $result = array('flag' => '1', 'message' => 'Successfully edited item!', 'url' => 'item.html');
            }
            else
            {
                $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'item');
            }   
        }
       else
       {
            $result = array('flag' => '3', 'message' => 'Must fill all the fields', 'url' => 'item');
       }
    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in editing item', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>