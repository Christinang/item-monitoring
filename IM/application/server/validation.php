<?php
    include_once '../configuration/dbconnect.php';

    function _check_category_duplication($conn,$description)
    {
        $query = $conn->prepare('SELECT * FROM category WHERE category_name = "'.$description.'";');
        $query->execute();
        return ($query->rowCount() == 0) ? false:true;
    }

    function _check_item_duplication($conn,$name,$description,$price,$category_select_option)
    {
        $query = $conn->prepare('SELECT * FROM item WHERE item_name = "'.$name.'" and item_description = "'.$description.'" and item_price = "'.$price.'" and category_id = "'.$category_select_option.'";');
        $query->execute();
        return ($query->rowCount() == 0) ? false:true;
    }

	function _check_usr_duplication($conn,$name,$username,$pass)
    {
        $query = $conn->prepare('SELECT * FROM usr WHERE usr_fullname = "'.$name.'" and usr_username = "'.$username.'" and usr_password = "'.$pass.'";');
        $query->execute();
        return ($query->rowCount() == 0) ? false:true;
    }

    function _check_supplier_duplication($conn,$name,$address,$contactNo)
    {
        $query = $conn->prepare('SELECT * FROM supplier WHERE sup_name = "'.$name.'" and sup_address = "'.$address.'" and sup_contactNo = "'.$contactNo.'";');
        $query->execute();
        return ($query->rowCount() == 0) ? false:true;
    }


?>