<?php
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    

    try
    {
        $query = $conn->prepare('SELECT * from SUPPLIER');
        $query->execute();
        $check = $query->setFetchMode(PDO::FETCH_ASSOC); 
        if($check == true)
        {
            $row = $query->fetchAll();
            if($_GET['type'] == 'table')
            {
                _displayTable($row);
            }
            else if($_GET['type'] == 'option')
            {
                echo json_encode($row);
            }
        }


    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Oops', 'url' => '');
        // echo $e->getMessage();
    }

    function _displayTable($row)
    {
        echo '
        <table id="listofSupplier" class="table table-bordered table-nowrap dataTable">
            
            <thead>
                <tr>
                    <th>Supplier Name</th>
                    <th>Address</th>
                    <th>Contact No.</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach($row as $k=>$v) {
            $temp = $v['sup_id']; 
            echo '
             <tr>
                <td>'.$v['sup_name'].'</td>
                <td>'.$v['sup_address'].'</td>
                <td>'.$v['sup_contactNo'].'</td>
                <td>
                    <div>
                        <button type="submit" name="edit" id="'.$temp.'" data-toggle="tooltip" data-placement="top" title="Edit" onclick="openModal('.$temp.')" class="btn btn-warning btn-sm edit_supplier"><i class="icon icon-pencil"></i> Edit</button>
                    </div>
                </td>
            </tr>';
        }

        echo '
            </tbody>
        </table>
        ';
    }
    
?>