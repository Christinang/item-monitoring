<?php
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    

    try
    {
        $query = $conn->prepare('SELECT * from displayCategory');
        $query->execute();
        $check = $query->setFetchMode(PDO::FETCH_ASSOC); 
        if($check == true)
        {
            $row = $query->fetchAll();
            if($_GET['type'] == 'table')
            {
                _displayTable($row);
            }
            else if($_GET['type'] == 'option')
            {
                echo json_encode($row);
            }
        }


    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Oops', 'url' => '');
        // echo $e->getMessage();
    }

    function _displayTable($row)
    {
        echo '
        <table id="listofCategory" class="table table-bordered table-nowrap dataTable">
            
            <thead>
                <tr>
                    <th>Category Name</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach($row as $k=>$v) { 
            $temp = $v['category_id'];
            echo '
             <tr>
                <td>'.$v['category_name'].'</td>
                <td>'.$v['usr_fullname'].'</td>
                <td>
                    <div>
                        <button type="submit" name="edit" id="'.$temp.'" data-toggle="tooltip" data-placement="top" title="Edit" onclick="openModal('.$temp.')" class="btn btn-warning btn-sm edit_category"><i class="icon icon-pencil"></i> Edit</button>
                    </div>
                </td>
            </tr>';
        }

        echo '
            </tbody>
        </table>
        ';
    }
    
?>