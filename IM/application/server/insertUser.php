<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $user_fullname = $_POST['name'];
    $user_usrname = $_POST['username'];
    $user_password = $_POST['pass'];

    try
    {
        $query = $conn->prepare('INSERT into USR (usr_id,usr_fullname,usr_username,usr_password) VALUES(null,"'.$user_fullname.'","'.$user_usrname.'","'.$user_password.'")');
        if(!_check_usr_duplication($conn,$user_fullname,$user_usrname,$user_password))
        {
            $query->execute();
            $result = array('flag' => '1', 'message' => 'Successfully added user!', 'url' => 'user.html');
        }
        else
        {
            $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'user');
        }
    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in adding user', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>