<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $item_name = $_POST['name'];
    $item_description = $_POST['description'];
    $item_price = $_POST['price'];
    $category_id = $_POST['category_select_option'];

    try
    {
        $query = $conn->prepare('INSERT into ITEM (item_id,category_id,item_name,item_description,item_price,created_by) VALUES(null,"'.$category_id.'","'.$item_name.'","'.$item_description.'","'.$item_price.'","'.$_SESSION['id'].'")');
        if(!_check_item_duplication($conn,$item_name,$item_description,$item_price,$category_id))
        {
            $query->execute();
            $result = array('flag' => '1', 'message' => 'Successfully added item!', 'url' => 'item.html');
        }
        else
        {
            $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'item');
        }
    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in adding item', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>