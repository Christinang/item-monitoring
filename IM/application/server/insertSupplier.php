<?php
    session_start();
    include_once '../configuration/dbconnect.php';
    include 'validation.php';
    $result = array();
    $sup_name = $_POST['name'];
    $sup_address = $_POST['address'];
    $sup_contactNo = $_POST['contactNo'];

    try
    {
        $query = $conn->prepare('INSERT into SUPPLIER (sup_id,sup_name,sup_address,sup_contactNo) VALUES(null,"'.$sup_name.'","'.$sup_address.'","'.$sup_contactNo.'")');
        if(!_check_category_duplication($conn,$sup_name,$sup_address,$sup_contactNo))
        {
            $query->execute();
            $result = array('flag' => '1', 'message' => 'Successfully added category!', 'url' => 'category.html');
        }
        else
        {
            $result = array('flag' => '2', 'message' => 'Already in the list', 'url' => 'category');
        }   
    }
    catch(PDOException $e)
    {
        $result = array('flag' => '0', 'message' => 'Error in adding category', 'url' => '');
        // echo $e->getMessage();
    }

    echo json_encode($result);
?>