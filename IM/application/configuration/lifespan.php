<?php
    session_start();

    if(isset($_SESSION['id']))
    {
        if((time() - $_SESSION['time_start_login']) > 3600)
        {
            header("location: ../application/server/logout.php");
        } else 
        {
            $_SESSION['time_start_login'] = time();
        }
    } else 
    {
        // header("location: ../application/server/logout.php");
    }
 ?>